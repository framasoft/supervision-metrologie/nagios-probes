#!/usr/bin/env bash

set -euo pipefail

URLS=urls.list
FALS=false_positives.list

while getopts u:f:c: opt; do
    case ${opt} in
        "u")
            URLS=$OPTARG
            ;;
        "f")
            FALS=$OPTARG
            ;;
        "c")
            CACHEDIR=$OPTARG
            ;;
        *)
            exit 3
            ;;
    esac
done

if [[ ! -r $URLS ]]; then
    echo "No readable URL file (searching for $URLS). Aborting."
    exit 1
fi

if [[ ! -e $FALS ]]; then
    if [[ ! -r $FALS ]]; then
        echo "Can’t read $FALS file. Aborting."
        exit 2
    else
        FALSE_POSITIVE_REGEX='^$'
    fi
else
    FALSE_POSITIVE_REGEX=$(tr '\n' '|' <"$FALS" | sed -e 's@|$@@')
fi

if [[ ! -d $CACHEDIR ]]; then
    mkdir "$CACHEDIR"
fi

now=$(date +%s)
while IFS= read -r url; do
    md5=$(echo "$url" | md5sum | sed -e "s/ .*//")
    file="$CACHEDIR/$md5.txt"

    # Remove old file
    if [[ -e $file ]]; then
        age=$(stat --format %W "$file")
        diff=$((now - age))
        if [[ $diff -gt 3600 ]]; then
            rm "$file"
        fi
    fi

    if [[ ! -e $file ]]; then
        echo -e "$url\n===============" >"$file"
        curl -fsSL "$url" >>"$file"
    fi
done <"$URLS"

match=0
lists=""
for list in "$CACHEDIR"/*.txt; do
    set +e
    case $list in
        # https://raw.githubusercontent.com/mhhakim/pihole-blocklist/refs/heads/master/porn.txt
        # Already known
        "$CACHEDIR/b621cc4548253d05d859f9ba246053fd.txt")
            EXTRA="|stats.framasoft.org"
            ;;
        # https://raw.githubusercontent.com/columndeeply/hosts/main/hosts02
        # Already known
        "$CACHEDIR/f4db40aeafcf5039577eaafca2e7e48d.txt")
            EXTRA="|stats.framasoft.org"
            ;;
        # https://airvpn.org/api/dns_lists/?code=oisd_full_dblw&style=domains
        # Telemetry/Analytics/Tracking block list
        "$CACHEDIR/cb5ebe1388ed970e1e3614708ea4639c.txt")
            EXTRA="|stats.framasoft.org"
            ;;
        *)
            EXTRA=""
            ;;
    esac
    count=$(grep -vP "$FALSE_POSITIVE_REGEX$EXTRA" "$list" | grep -c "frama\|framindmap")
    set -e
    if [[ $count -ne 0 ]]; then
        lists="$lists $(head -n 1 "$list"):$count"
    fi
    match=$((match + count))
done

if [[ $match -ne 0 ]]; then
    echo "Watch blocking lists FAILED - $match matches |$lists"
    exit 2
else
    echo "Watch blocking lists OK — $match match |"
    exit 0
fi
