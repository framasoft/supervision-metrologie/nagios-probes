#!/bin/bash

export NAGIOS=0
while getopts ':n' arg
do
    case ${arg} in
        n)
            export NAGIOS=1
            ;;
    esac
done

COUNT=$(echo "SELECT count(e.name) FROM emoji e JOIN users u ON u.id = e.creatorid WHERE e.creatorid NOT IN ( SELECT userid FROM teammembers tm JOIN teams t ON tm.teamid = t.id WHERE t.name = 'framasoft') AND e.deleteat = 0;" | sudo -u gitlab-psql -- /opt/gitlab/embedded/bin/psql -h /var/opt/gitlab/postgresql/ -t mattermost_production | head -n 1 | sed "s/ //g")

if [[ $COUNT -ne 0 ]]
then
    if [[ $NAGIOS -ne 0 ]]
    then
        echo "Watch emojis FAILED - $COUNT unauthorized custom emojis | unauthorized=$COUNT"
        exit 2
    else
        echo "SELECT e.name as emoji_name, to_timestamp(e.createat/1000) AS created_at, u.username AS creator FROM emoji e JOIN users u ON u.id = e.creatorid WHERE e.creatorid NOT IN ( SELECT userid FROM teammembers tm JOIN teams t ON tm.teamid = t.id WHERE t.name = 'framasoft') AND e.deleteat = 0 ORDER BY u.username;" | sudo -u gitlab-psql -- /opt/gitlab/embedded/bin/psql -h /var/opt/gitlab/postgresql/ mattermost_production | mail -s "[Alerte Framateam] Des émojis custom ont été créés par des utilisatrices !" tech@framalistes.org
    fi
fi

if [[ $NAGIOS -ne 0 ]]
then
    echo "Watch emojis OK - $COUNT unauthorized custom emojis | unauthorized=$COUNT"
    exit 0
fi
