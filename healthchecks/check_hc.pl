#!/usr/bin/env perl
use Mojo::Base -strict;
use Mojo::UserAgent;
use Mojo::Collection 'c';
use Getopt::Long;

my ($url, $help);

GetOptions("url|u=s" => \$url,
           "help|h"  => \$help
);
if ($help or !$url) {
    say "Please, use the --url option.\n" if (!$url && !$help);
    usage();
}

my $ua = Mojo::UserAgent->new();

my $res = $ua->get($url)->res->body;

unless ($res) {
    say "HealthChecks UNKNOWN - unable to get the data";
    exit 3;
}

my $lines = c(split("\n", $res));

my $total = $lines->grep(qr/^hc_checks_total \d+/)
                  ->map(sub { $_ =~ m/^hc_checks_total (\d+)/;      return $1; })
                  ->first;
my $downs = $lines->grep(qr/^hc_checks_down_total \d+/)
                  ->map(sub { $_ =~ m/^hc_checks_down_total (\d+)/; return $1; })
                  ->first;

my $ups = $total - $downs;

if ($downs) {
    my $down_names = $lines->grep(qr/^hc_check_up\{.*\} 0/)
                           ->map(sub { $_ =~ m/^hc_check_up\{name="([^"]+)".*\} 0/; return $1; })
                           ->to_array;
    my $down_text = join(', ', @{$down_names});
    say "HealthChecks FAILED - $downs checks down on $total checks ($down_text) | down=$downs up=$ups total=$total";
    exit 2;
} else {
    say "HealthChecks OK - $downs checks down on $total checks | down=$downs up=$ups total=$total";
    exit 0;
}

sub usage {
    print <<EOF;
(c) Framasoft 2021, licensed under the terms of the WTFPL

Usage:
  check_hc.pl [--url|-u <HealthChecks Prometheus metrics endpoint>] [--help|-h]

Options:
  --url|-u   Specifies the HealthChecks Prometheus metrics endpoint to fetch
             Mandatory
  --help|-h  Print this help and exit

Dependencies:
  You will need the Mojolicious web framework:
    apt install libmojolicious-perl
  Or:
    cpan Mojolicious
EOF

    exit 3;
}
